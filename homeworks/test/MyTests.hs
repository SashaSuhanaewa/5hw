-- Your tests go here

module MyTests where

import Test.Tasty (TestTree(..), testGroup)
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Lib

myTests :: [TestTree]
myTests = [ testGroup "HW0"
            [ testCase "Works on Alice" $ hw0_0 "Alice" @?= "Hello, Alice" ]
          ]

